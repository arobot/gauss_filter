This is an example C++ library for gaussian filtering by Andres Flores Valle

Installation
--------------------------------------------------------------------------------

To install this library, just place this entire folder as a subfolder in your
Arduino/libraries/ folder.

When installed, this library should look like:

Arduino/libraries/gauss_filter              (this library's folder)
Arduino/libraries/gauss_filter/src/         (the source folder)
Arduino/libraries/guass_filter/include/     (the header folder)
Arduino/libraries/gauss_filter/keywords.txt (the syntax coloring of the library)
Arduino/libraries/gauss_filter/examples     (the examples in the "open" menu)
Arduino/libraries/gauss_filter/readme.txt   (this file)

Building
--------------------------------------------------------------------------------

After this library is installed, you just have to start the Arduino application.
You may see a few warning messages as it's built.

To use this library in a sketch, go to the Sketch | Import Library menu and
select gauss_filter.  This will add a corresponding line to the top of your sketch:
#include <gauss_filter.h>

To stop using this library, delete that line from your sketch.

Geeky information:
After a successful build of this library, a new file named "Test.o" will appear
in "Arduino/libraries/gauss_filter". This file is the built/compiled library
code.

If you choose to modify the code for this library (i.e. "GaussFilter.cpp" or "GaussFilter.h"),
then you must first 'unbuild' this library by deleting the "gauss_filter.o" file. The
new "gauss_filter.o" with your code will appear after the next press of "verify"


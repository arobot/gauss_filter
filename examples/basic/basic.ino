#include "GaussFilter.h"

float var;                  // Define your variable to filter
float sigma = 2;            // Define sigma
float sample_num = 3;       // Give the number of samples (actually it is 2*sample_num + 1)
GaussFilter filter(&var, sample_num, sigma);   // Declare Gaussian Filter

void setup() {
  Serial.begin(9600);
}

void loop() {
  var = 1;                             // Set the variable
  float var_smooth = filter.smooth();  // Smooth the variable
  Serial.println(var_smooth);
  delay(1000);

}

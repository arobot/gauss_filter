/*
 * GaussFilter.cpp
 *
 *  Created on: Oct 4, 2017
 *      Author: Andres
 */

#include "GaussFilter.h"


GaussFilter::GaussFilter(float* variable, int SampleNum, float Sigma) {
	this->var = variable;
	this->sample_num = SampleNum;
	this->sigma = Sigma;
	this->arr_var = new float[2*this->sample_num + 1];
	for(int i=0; i < 2*this->sample_num + 1; i++){
		this->arr_var[i] = 0;
	}
}

GaussFilter::~GaussFilter() {
	delete this->arr_var;
	this->arr_var = NULL;
}



float GaussFilter::smooth(){
	float filter_var = 0;
	for(int i=0; i < 2*this->sample_num + 1; i++){
		if(i < 2*this->sample_num){
			this->arr_var[i] = this->arr_var[i+1];
		}
		else if(i == 2*this->sample_num){
			this->arr_var[i] = *this->var;
		}
		filter_var += this->arr_var[i]*this->gauss_factor(i);
	}
	return filter_var;
}


float GaussFilter::gauss_factor(int x){
	x = x - this->sample_num;
	float pot = - pow(x,2)/(2*pow(this->sigma, 2));
	float factor = (1/(pow(2*_PI, 0.5)*this->sigma)) * pow(_e, pot);
	return factor;
}

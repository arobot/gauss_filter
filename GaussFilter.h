/*
 * GaussFilter.h
 *
 *  Created on: Oct 4, 2017
 *      Author: andres
 */

#ifndef GAUSSFILTER_H_
#define GAUSSFILTER_H_

#include "Arduino.h"

#define _PI 3.14159264
#define _e  2.71828182


/**
 * GaussFilter Class:
 * Arguments:
 * 		float* variable: It is the pointer of the variable that you want to smooth
 * 		int SampleNum:   This is the number of samples used to smooth. This value is taken
 * 		                 as 2*SampleNum + 1, since it must be odd.
 * 		float sigma:     This is the parameter of the gaussian filter
 *
 */
class GaussFilter {
public:
	GaussFilter(float* variable, int SampleNum, float Sigma);
	~GaussFilter();

	float smooth();

private:
	float gauss_factor(int x);

	float sigma;
	int sample_num;
	float* var;
	float* arr_var;
};

#endif /* GAUSSFILTER_H_ */
